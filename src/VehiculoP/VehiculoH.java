/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VehiculoP;

/**
 *
 * @author Valeria
 */
public class VehiculoH {
   public String Duenno;
   public String Marca;
   public String Color;
   public String Tipo;
   public double Precio;
public VehiculoH(String Duenno,String Marca,String Color,String Tipo,double Precio){
    this.Duenno = Duenno;
    this.Marca = Marca;
    this.Color = Color;
    this.Tipo = Tipo;
    this.Precio = Precio;
}
    public String getDuenno() {
        return Duenno;
    }

    public void setDuenno(String Duenno) {
        this.Duenno = Duenno;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }
    
}
