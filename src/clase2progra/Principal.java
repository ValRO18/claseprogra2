/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2progra;

import java.util.Scanner;

/**
 *
 * @author Valeria
 */
public class Principal {

    public static void main(String[] args) {
        Logica log = new Logica();
        Scanner sc = new Scanner(System.in);
        String menu = "1.Juego"+"\n"+"2.Personas"+"\n"+"3.Palabras que Rima";
        System.out.println(menu+"\n"+"Seleccione una opcion:");
        int elecc = Integer.parseInt(sc.nextLine());
        while(true){
            if(elecc == 1){
                System.out.println("Adivina el numero!");
                int numeroAl = (int) (Math.random() * 20) + 1;
                System.out.println(numeroAl);
                int numero = 0;
                int intentos = 5;
                boolean gano = false;
                if(intentos!=0){
                    for (int i =1; i <=intentos; i++) {
                        if(log.gano(gano,numero,numeroAl)!=true){
                            System.out.println("Digite un numero:");
                            numero = Integer.parseInt(sc.nextLine());
                            intentos--;
                            System.out.println("Intentos:" +intentos);
                            System.out.println(log.clasificarNum(numero, numeroAl));   
                        }
                        else if(log.gano(gano,numero,numeroAl) == true){
                            System.out.println("Felicidades gano el juego!");
                            break;
                        }  
                    }
                }
                else{
                    System.out.println("Pediste fin del juego!");
                }
            }
            else if(elecc == 2){
                
                String mimenu = "1.Agregar persona"+"\n"+"2.Modificar edad"+"\n"+"3.Ver matriz\n";
                System.out.println(mimenu +"Seleccione una opcion:");
                int op = Integer.parseInt(sc.nextLine());
                if(op == 1){
                    System.out.println("Desea agregar a alguna persona?"+"\n"+"1.Si"+"\n"+"2.No");
                        int ele = Integer.parseInt(sc.nextLine());
                        if(ele == 1){
                            System.out.println("Digite el nombre de la persona:");
                            String nombre = sc.nextLine();
                            System.out.println("Digite la edad de la persona:");
                            int edad =  Integer.parseInt(sc.nextLine());
                            log.agregarPersona(nombre,edad);
                        }
                        else{
                        }
                }
                else if(op == 2){
                    System.out.println("Digite el nombre de la persona que desea modificar:");
                    String nombres = sc.nextLine();
                    System.out.println("Digite la nueva edad:");
                    int eddad = Integer.parseInt(sc.nextLine());
                    log.modificarP(nombres,eddad);
                }
                else if(op==3){
                     log.verMatriz();
                }
                else{
                }
            }
            else if(elecc == 3){
                System.out.println("Digite una palabra:");
                String palabra1 = sc.nextLine();
                System.out.println("Digite una palabra:");
                String palabra2 = sc.nextLine();
                log.riman(palabra1,palabra2);    
            }
        }
    }
}
