/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class ConAire extends Congelados {
    
    public int canNitrogeno;
    public int canOxigeno;
    public int canDioCar;
    public int vaporAgua;
    
    public ConAire(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen,int temperatura,int canDioCar,int canNitrogeno,int canOxigeno,int vaporAgua){
     super(fechaCaducidad,numLote,fechaEnvasado,paisOrigen,temperatura);
        this.canDioCar = canDioCar;
        this.canNitrogeno = canNitrogeno;
        this.canOxigeno = canOxigeno;
        this.vaporAgua = vaporAgua;
    }
    public String getAtributos(){
         return "Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen+"\nTenperatura:"+temperatura;       
    }
}
