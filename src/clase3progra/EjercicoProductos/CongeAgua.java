/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class CongeAgua extends Congelados{
    public  int pSaliXLAgua;
    public CongeAgua(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen,int temperatura,int pSaliXLAgua){
        super(fechaCaducidad,numLote,fechaEnvasado,paisOrigen,temperatura);
        this.pSaliXLAgua = pSaliXLAgua;
        
    }
     public String getAtributos(){
         return "Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen+"\nTenperatura:"+temperatura+"\n% Salinidad x Lit de Agua:"+pSaliXLAgua;       
    }
    
    
}
