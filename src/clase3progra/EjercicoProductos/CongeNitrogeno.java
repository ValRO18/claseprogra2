/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class CongeNitrogeno  extends Congelados{
    public String metConge;
    public double timExpNitr;
    
    public CongeNitrogeno(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen,int temperatura,String metConge,double timExpNitr){
        super(fechaCaducidad,numLote,fechaEnvasado,paisOrigen,temperatura);
        this.metConge = metConge;
        this.timExpNitr =timExpNitr;
    }
    public String getAtributos(){
         return "Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen+"\nTenperatura:"+temperatura+"\nMetodo de Congelacion:"+metConge+"\nTiempo Exposicion al nitrogeno:"+timExpNitr+" Seg";       
    }
    
    
}
