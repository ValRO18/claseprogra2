/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class Empresa {
    public static void main(String[] args) {
        System.out.println("Frescos");
        Frescos pro1Fre = new Frescos("18/09/2018",43,"11/10/2018","Canada");
        System.out.println(pro1Fre.getAtributos());
        System.out.println("\n");
        System.out.println("Refregerados");
        Refrigerados pro1Refri = new Refrigerados("12/03/2019",20,"1/02/2019","Costa Rica",33445,32);
        System.out.println(pro1Refri.getAtributos());
        System.out.println("\n");
        System.out.println("Congelados");
        Congelados con1 = new Congelados("20/12/2020",33,"01/05/2019","USA",20);
        System.out.println(con1.getAtributos());
        System.out.println("\n");
        System.out.println("Congelados con Aire");
        ConAire coAire1 = new ConAire("03/04/2018",12,"09/10/2022","China",20,10,10,20,15);
        System.out.println(coAire1.getAtributos());
        System.out.println("\n");
        System.out.println("Congelados con Agua");
        CongeAgua coAgua1 = new CongeAgua("07/05/2019",889,"09/10/2021","Rusia",19,2);
        System.out.println(coAgua1.getAtributos());
        System.out.println("\n");
        System.out.println("Congelados con Nitrogeno");
        CongeNitrogeno coNitro1 = new CongeNitrogeno("17/05/2019",69,"29/10/2021","Rusia",19,"Nitrogeno",3.4);  
        System.out.println(coNitro1.getAtributos());
        System.out.println("\n");
    } 
}
