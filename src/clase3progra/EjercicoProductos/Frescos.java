/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class Frescos extends Productos{
    public Frescos(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen){
        super(fechaCaducidad,numLote,fechaEnvasado,paisOrigen);
    }
    public  String getAtributos(){
         return "Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen;       
    }
    
    
}
