/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class Productos {
    public String fechaCaducidad;
    public  int numLote;
    public String fechaEnvasado;
    public String paisOrigen;

    public Productos(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen){
        this.fechaCaducidad = fechaCaducidad;
        this.numLote  = numLote;
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;         
    }
    public String getAtributos(){
        return "Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen;       
    }
    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public String getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(String fechaEnvasado) {
        this.fechaEnvasado = fechaEnvasado;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }
    
}
