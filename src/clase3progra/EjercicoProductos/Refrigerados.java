/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra.EjercicoProductos;

/**
 *
 * @author Valeria
 */
public class Refrigerados extends Productos{
    public int codOrgaAli;
    public int temperatura;

    
    
    public Refrigerados(String fechaCaducidad,int numLote,String fechaEnvasado,String paisOrigen,int codOrgaAli,int temperatura){
        super(fechaCaducidad,numLote,fechaEnvasado,paisOrigen);
        this.codOrgaAli = codOrgaAli;
        this.temperatura = temperatura;
    }
    public String getAtributos(){
        return"Fecha Caducidad:"+fechaCaducidad+"\nNumero de Lote:"+numLote+"\nFecha de Envasado:"+fechaEnvasado+"\nPais de Origen:"+paisOrigen+"\nCodigo organismo alimentario:"+codOrgaAli+"\nTemperatura:"+temperatura;
    }
    
    public void Temperatura(){
        System.out.println("La temperatura es:");
    }
    public int getCodOrgaAli() {
        return codOrgaAli;
    }

    public void setCodOrgaAli(int codOrgaAli) {
        this.codOrgaAli = codOrgaAli;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
}
