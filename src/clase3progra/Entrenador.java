/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra;

/**
 *
 * @author Valeria
 */
public class Entrenador  extends Equipo{
     public int idFederacion;
     public boolean dirigirPar;
     public boolean dirigirEntre;

   
     public Entrenador(int id,String nombre,String apellido,int edad,boolean viaja,boolean concentrarse,int idFederacion,boolean dirigirPar,boolean dirigirEntre){
         super(id,nombre,apellido,edad,viaja,concentrarse);
         this.idFederacion = idFederacion;
         this.dirigirPar = dirigirPar;
         this.dirigirEntre = dirigirEntre;
     }
     public String getAtributos() {
         return "Id:"+id+"\nNombre:"+nombre+"\nApellido:"+apellido+"\nEdad:"+edad+"\nId Federacion:"+idFederacion+"\nViaja:"+viaja+"\nConcentarse:"+concentrarse+"\nDirige el partido:"+dirigirPar+"\nDirige entrenamiento:"+dirigirEntre;
     }

    public int getIdFederacion() {
        return idFederacion;
    }

    public void setIdFederacion(int idFederacion) {
        this.idFederacion = idFederacion;
    }
     public boolean isDirigirPar() {
        return dirigirPar;
    }

    public void setDirigirPar(boolean dirigirPar) {
        this.dirigirPar = dirigirPar;
    }

    public boolean isDirigirEntre() {
        return dirigirEntre;
    }

    public void setDirigirEntre(boolean dirigirEntre) {
        this.dirigirEntre = dirigirEntre;
    }
    
    public void dirigirPartido(boolean dirigirPar){
        System.out.println("Dirige el partido:"+dirigirPar);  
    }
     
    public void dirigirEntrena(boolean dirigirEntre){
        System.out.println("Dirige el entrenamiento:"+dirigirEntre);  
    }
     
     
  
    
}
