/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra;

/**
 *
 * @author Valeria
 */
public class Equipo {
    public  int id;
    public String nombre;
    public String apellido;
    public int edad;
    public boolean viaja;
    public boolean concentrarse;

    
    public Equipo(int id, String nombre, String apellido,int edad,boolean viaja,boolean concentrarse){
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.viaja = viaja;
        this.concentrarse = concentrarse;
        
    }
     public String getATributos(){
         return " Id:"+id+"\nNombre:"+nombre+"\nApellido:"+apellido+"\nEdad:"+edad+"\nViaja:"+viaja+"\nConcentrarse:"+concentrarse;
     }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public boolean isViaja() {
        return viaja;
    }

    public void setViaja(boolean viaja) {
        this.viaja = viaja;
    }

    public boolean isConcentrarse() {
        return concentrarse;
    }

    public void setConcentrarse(boolean concentrarse) {
        this.concentrarse = concentrarse;
    }
    
    
}
