/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra;

/**
 *
 * @author Valeria
 */
public class Futbolista  extends Equipo{
    public int dorsal;
    public String demarcacion;
    
    public Futbolista(int id,String nombre,String apellido,int edad,boolean viaja,boolean concentrarse,int dorsal,String demarcacion){
        super(id,nombre,apellido,edad,viaja,concentrarse);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    public String getAtributos(){
         return "Id:"+id+"\nNombre:"+nombre+"\nApellido:"+apellido+"\nEdad:"+edad+"\nViaja:"+viaja+"\nConcentarse:"+concentrarse +"\nDorsal;"+dorsal+"\nDemarcacion:"+demarcacion;    }
    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getDemarcacion() {
        return demarcacion;
    }

    public void setDemarcacion(String demarcacion) {
        this.demarcacion = demarcacion;
    }
         
    
}
