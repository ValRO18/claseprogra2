/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3progra;

/**
 *
 * @author Valeria
 */
public class Masajista extends Equipo {
    public  String titulacion;
    public int anosExperiencia;
    
    public Masajista(int id,String nombre,String apellido,int edad,boolean viaja,boolean concentrarse,String titulacion,int anosExperiencia){
      super(id,nombre,apellido,edad,viaja,concentrarse);
      this.titulacion = titulacion;
      this.anosExperiencia = anosExperiencia;
     }
    public  String getAtributos(){
        return"Id:"+id+"\nNombre:"+nombre+"\nApellido:"+apellido+"\nEdad:"+edad+"\nViaja:"+viaja+"\nConentrarse:"+concentrarse+"\nTitulacion:"+titulacion+"\nAños Experiencia:"+anosExperiencia;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    public int getAnosExperiencia() {
        return anosExperiencia;
    }

    public void setAnosExperiencia(int anosExperiencia) {
        this.anosExperiencia = anosExperiencia;
    }
            
    
}
